<?php
/**
 * Plugin Name: "Modabrand" products
 * Description: This is CRUD plugin for products and checking it for showing in front
 * Author: Andrew Puzirevich
 * Version: 1.0
 */

function addProducts() {

	global $wpdb;

	if ( isset( $_POST['addProd'] ) ) {
		$name_ua   = clean( $_POST['ua_n'] );
		$name_ru   = clean( $_POST['ru_n'] );
		$alt       = clean( $_POST['alt'] );
		$descr_seo = clean( $_POST['seo_descr'] );
		$descr     = $_POST['descr'];
		$cat       = $_POST['cat'];
		$subCat    = $_POST['Subcat'];
		$brand     = $_POST['brand'];
		$price     = $_POST['price'];
		$sale      = $_POST['sale'];
		$article   = $_POST['article'];
		$sizes     = serialize( $_POST['size'] );
		$wSizes    = serialize( $_POST['w'] );
		$available = 0;
		$country   = $_POST['country'];
		$showcase  = 0;

		$images = array();

		if ( $_POST['available'] == 'on' ) {
			$available = 1;
		}
		if ( $_POST['showcase'] == 'on' ) {
			$showcase = 1;
		}

// Изменим структуру $_FILES
		foreach ( $_FILES['img'] as $key => $value ) {
			foreach ( $value as $k => $v ) {
				$_FILES['img'][ $k ][ $key ] = $v;
			}

			// Удалим старые ключи
			unset( $_FILES['img'][ $key ] );
		}
		$iterat = 0;
		foreach ( $_FILES['img'] as $k => $v ) {
			$_FILES['img'][ $k ]['tmp_name'];
			$_FILES['img'][ $k ]['error'];
			if ( $_FILES['img'][ $k ]['error'] == 0 ) {
				$type     = explode( ".", $_FILES['img'][ $k ]['name'] );
				$img      = date( 'y-m-d--h-m-s-' ) . rand(1,800) . '.jpg';
				$tmp_path = $_FILES['img'][ $k ]['tmp_name'];
				if ( is_image( $tmp_path ) == true ) {
					$cat_dir    = $cat . '/';
					$uploaddir  = '../wp-content/themes/twentynineteen/productsImg/categories/' . $cat_dir;
					$uploadfile = $uploaddir . basename( $img );
					if ( ! is_dir( $uploaddir ) ) {
						mkdir( $uploaddir );
					}
					if ( move_uploaded_file( $tmp_path, $uploadfile ) ) {
						array_push( $images, $cat_dir . basename( $img ) );
						$iterat ++;
					}
				} else {
					echo "<script>alert('Загруженные файлы не соответствует формату картинки');</script>";
				}
			}
		}
		$images = serialize( $images );

		if ( $subCat ) {
			$query  = "SELECT post_id FROM `sub_category` WHERE id='$subCat'";
			$parent = $wpdb->get_row( $query );
			$parent = $parent->post_id;
		} else {
			$query  = "SELECT post_id FROM `mb_category` WHERE id='$cat'";
			$parent = $wpdb->get_row( $query );
			$parent = $parent->post_id;
		}

		$query = "INSERT INTO `mb_products`(id_cat, id_sub_cat, name_ru, name_ua, brand, description, price, sale, article, size_list, alt, descr_seo, photo, w_size, available,country,showcase) VALUES ('$cat','$subCat','$name_ru','$name_ua','$brand','$descr','$price', '$sale', '$article','$sizes','$alt','$descr_seo','$images','$wSizes', '$available','$country','$showcase')";
		$wpdb->query( $query );
		$lastId = $wpdb->insert_id;
		$postName = $lastId.'-'. $name_ru;

		$page_check = get_page_by_title( transliterate( $postName ) );
		$new_page   = array(
			'post_type'    => 'page',
			'post_title'   => $name_ru,
			'post_name'    => transliterate( $postName ),
			'post_content' => '',
			'post_status'  => 'publish',
			'post_author'  => 1,
			'post_parent'  => $parent,
		);
		if ( ! isset( $page_check->ID ) ) {
			$new_page_id = wp_insert_post( $new_page );
			update_post_meta( $new_page_id, '_wp_page_template', 'product.php' );
			$page  = get_post( $new_page_id );
			$slug  = $page->post_name . '/';
			$query = "UPDATE `mb_products` SET url_ru='$slug' WHERE id='$lastId'";
			if ( $wpdb->query( $query ) ) {
				echo "<script>Swal.fire(
  'Успех!',
  'Позиция обновлена!',
  'success'
)</script>";
			}

		}

	}

	$query          = "SELECT id,name_ru FROM `mb_category`";
	$categoriesList = $wpdb->get_results( $query );

	$query  = "SELECT * FROM `mb_brands` ORDER BY id";
	$brands = $wpdb->get_results( $query );

	$query             = "SELECT id,name_ru FROM `sub_category`";
	$subCategoriesList = $wpdb->get_results( $query );
	?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?= get_template_directory_uri() ?>/js/category.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <!-- Optional: include a polyfill for ES6 Promises for IE11 and Android browser -->
    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
    <div>
        <h3>Добавление товара</h3>
        <form style="width: 50%" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label>Укажите категорию</label>
                <select name="cat" id="" required>
                    <option selected="selected"></option>
					<?php
					foreach ( $categoriesList as $item ):
						?>
                        <option value="<?php echo $item->id; ?>"><?php echo $item->name_ru; ?></option>
					<?php
					endforeach;
					?>
                </select>
            </div>
            <div class="form-group">
                <label>Укажите подкатегорию</label>
                <select name="Subcat" id="subCat">
                    <option selected="selected"></option>
					<?php
					foreach ( $subCategoriesList as $item ):
						?>
                        <!--                    <option selected="selected"></option>-->
                        <option value="<?php echo $item->id; ?>"><?php echo $item->name_ru; ?></option>
					<?php
					endforeach;
					?>
                </select>
            </div>
            <div class="form-group">
                <label>Название (рус.)<input type="text" name="ru_n" required></label>
                <label>Название (укр.)<input type="text" name="ua_n" required></label>
            </div>
            <div class="form-group">
                <label>Фотография <input type="file" name="img[]" multiple></label>
            </div>
            <div class="form-group">
                <label>Alt (для первого фото)</label><br>
                <textarea name="alt" id="" cols="60" rows="5" style="width: 100%;"></textarea>
            </div>
            <div class="form-group">
                <label>Description (SEO описание товара)</label><br>
                <textarea name="seo_descr" id="" cols="60" rows="5" style="width: 100%;"></textarea>
            </div>
            <div class="form-group">
                <label>Описание товара</label><br>
				<?php wp_editor( '', 'wpeditorprod', array( 'textarea_name' => 'descr' ) ); ?>
            </div>
            <div class="form-group">
                <label>Бренд </label>
                <select name="brand">
					<?php
					foreach ( $brands as $item ):
						?>
                        <option value="<?php echo $item->id; ?>"><?php echo $item->name; ?></option>
					<?php
					endforeach;
					?>
                </select>
            </div>
            <div class="form_group">
                <label>Страна производитель <input type="text" name="country"></label>
            </div>
            <div class="form-group">
                <label>Цена <input type="text" name="price"></label>
            </div>
            <div class="form-group">
                <label>Скидка в % <input type="text" name="sale"></label>
            </div>
            <div class="form-group">
                <label>Артикул <input type="text" name="article"></label>
            </div>
            <div class="form-group">
                <h6>Размеры</h6>
                <p>
                    <label>XS<br><input type="checkbox" name="size[]" value="XS"></label>
                    <label>S<br><input type="checkbox" name="size[]" value="S"></label>
                    <label>M<br><input type="checkbox" name="size[]" value="M"></label>
                    <label>L<br><input type="checkbox" name="size[]" value="L"></label>
                    <label>XL<br><input type="checkbox" name="size[]" value="XL"></label>
                    <label>2XL<br><input type="checkbox" name="size[]" value="2XL"></label>
                    <label>3XL<br><input type="checkbox" name="size[]" value="3XL"></label>
                    <label>4Xl<br><input type="checkbox" name="size[]" value="4Xl"></label>
                    <label>5XL<br><input type="checkbox" name="size[]" value="5XL"></label>
                    <label>6XL<br><input type="checkbox" name="size[]" value="6XL"></label>
                    <label>7XL<br><input type="checkbox" name="size[]" value="7XL"></label>
                    <label>8XL<br><input type="checkbox" name="size[]" value="8XL"></label>
                </p>

                <p>
                    <label>W:</label>
                    <label>27<br><input type="checkbox" name="w[]" value="27"></label>
                    <label>28<br><input type="checkbox" name="w[]" value="28"></label>
                    <label>29<br><input type="checkbox" name="w[]" value="29"></label>
                    <label>30<br><input type="checkbox" name="w[]" value="30"></label>
                    <label>31<br><input type="checkbox" name="w[]" value="31"></label>
                    <label>32<br><input type="checkbox" name="w[]" value="32"></label>
                    <label>33<br><input type="checkbox" name="w[]" value="33"></label>
                    <label>34<br><input type="checkbox" name="w[]" value="34"></label>
                    <label>36<br><input type="checkbox" name="w[]" value="36"></label>
                    <label>38<br><input type="checkbox" name="w[]" value="38"></label>
                    <label>40<br><input type="checkbox" name="w[]" value="40"></label>
                    <label>42<br><input type="checkbox" name="w[]" value="42"></label>
                    <label>44<br><input type="checkbox" name="w[]" value="44"></label>
                    <label>46<br><input type="checkbox" name="w[]" value="46"></label>
                    <label>48<br><input type="checkbox" name="w[]" value="48"></label>
                    <label>50<br><input type="checkbox" name="w[]" value="50"></label>
                    <label>52<br><input type="checkbox" name="w[]" value="52"></label>
                    <label>54<br><input type="checkbox" name="w[]" value="54"></label>
                    <label>56<br><input type="checkbox" name="w[]" value="56"></label>
                    <label>58<br><input type="checkbox" name="w[]" value="58"></label>
                    <label>60<br><input type="checkbox" name="w[]" value="60"></label>
                    <label>62<br><input type="checkbox" name="w[]" value="62"></label>
                    <label>64<br><input type="checkbox" name="w[]" value="64"></label>
                    <label>66<br><input type="checkbox" name="w[]" value="66"></label>
                </p>
            </div>
            <div class="form-group">
                <label for="">В наличии <input type="checkbox" name="available"></label>
            </div>
            <div class="form-group">
                <label for="">Добавить на витрину <input type="checkbox" name="showcase"></label>
            </div>
            <button type="submit" class="btn btn-success" name="addProd">Сохранить</button>
        </form>
    </div>
    <script>


    </script>
	<?php
}

function editProd() {
	global $wpdb;
	?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
	<?php

	if ( isset( $_POST['editProd'] ) ) {
		$id      = $_POST['editProd'];
		$name_ua = clean( $_POST['ua_n'] );
	    $name_ru   = clean( $_POST['ru_n'] );
		$alt       = clean( $_POST['alt'] );
		$descr_seo = clean( $_POST['seo_descr'] );
		$descr     = $_POST['descr'];
		$cat       = $_POST['cat'];
		$subCat    = $_POST['Subcat'];
		$brand     = $_POST['brand'];
		$price     = $_POST['price'];
		$sale      = $_POST['sale'];
		$article   = $_POST['article'];
	    $sizes     = serialize( $_POST['size'] );
	    $wSizes    = serialize( $_POST['w'] );
		$available = 0;
		$country   = $_POST['country'];
		$showcase  = 0;
		$images = array();

		if ( $_POST['available'] == 'on' ) {
			$available = 1;
		}
		if ( $_POST['showcase'] == 'on' ) {
			$showcase = 1;
		}
//ВЫНЕСТИ ЗАГРУЗКУ ФОТО В ФАНКШНС ПО АЯКСУ
// Изменим структуру $_FILES
        if ($_FILES['img']['name'][0] != ''){
	        foreach ( $_FILES['img'] as $key => $value ) {
		        foreach ( $value as $k => $v ) {
			        $_FILES['img'][ $k ][ $key ] = $v;
		        }

		        // Удалим старые ключи
		        unset( $_FILES['img'][ $key ] );
	        }
	        $iterat = 0;
	        foreach ( $_FILES['img'] as $k => $v ) {
		        $_FILES['img'][ $k ]['tmp_name'];
		        $_FILES['img'][ $k ]['error'];
		        if ( $_FILES['img'][ $k ]['error'] == 0 ) {
			        $type     = explode( ".", $_FILES['img'][ $k ]['name'] );
			        $img      = date( 'y-m-d--h-m-s-' ) . $iterat . '.' . $type[1];
			        $tmp_path = $_FILES['img'][ $k ]['tmp_name'];
			        if ( is_image( $tmp_path ) == true ) {
				        $cat_dir    = $cat . '/';
				        $uploaddir  = '../wp-content/themes/twentynineteen/productsImg/categories/' . $cat_dir;
				        $uploadfile = $uploaddir . basename( $img );
				        if ( ! is_dir( $uploaddir ) ) {
					        mkdir( $uploaddir );
				        }
				        if ( move_uploaded_file( $tmp_path, $uploadfile ) ) {
					        array_push( $images, $cat_dir . basename( $img ) );
					        $iterat ++;
				        }
			        } else {
				        echo "<script>alert('Загруженные файлы не соответствует формату картинки');</script>";
			        }
		        }
	        }
	        $images = serialize( $images );

	        $query = "SELECT photo FROM `mb_products` WHERE id='$id'";
	        $delP = $wpdb->get_row($query);
	        $delP = unserialize($delP->photo);
	        if ($delP){
	            foreach ($delP as $item){
	                unlink('../wp-content/themes/twentynineteen/productsImg/categories/'.$item);
                }
            }
            $query = "UPDATE `mb_products` SET photo='$images' WHERE id='$id'";
            $wpdb->query($query);
        }


        $time = date('Y-m-d H:m:s');
		$query = "UPDATE `mb_products` SET brand='$brand',description='$descr',price='$price',sale='$sale',article='$article',alt='$alt',descr_seo='$descr_seo',available='$available',country='$country',showcase='$showcase',size_list='$sizes',w_size='$wSizes', `date`='$time', name_ua='$name_ua', name_ru='$name_ru' WHERE id='$id'";
		if ( $wpdb->query( $query ) ) {
?>

            <?php
		}
		?>
        <div id="myModal" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Успех!</h4>
                    </div>
                    <div class="modal-body">Позиция обновлена!</div>
                    <div class="modal-footer" style="justify-content: center;"><a href="admin.php?<?= $_POST['back']; ?>" style="color: white; padding: 0.76rem 0.8rem; margin-bottom: 5px; background-color: #7cefbc; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;">Закрыть и вернуться на список</a></div>
                </div>
            </div>
        </div>
        <script>
            jQuery('#myModal').modal('toggle');
        </script>
        <?php
	}


	if ( isset( $_GET['id'] ) ) {

        $back = $_GET['back'];
		$searchA = $_GET['id'];

		$query   = "SELECT * FROM `mb_products` WHERE id='$searchA'";
		$product = $wpdb->get_row( $query );

		$query  = "SELECT * FROM `mb_brands` ORDER BY `name`";
		$brands = $wpdb->get_results( $query );

		$query          = "SELECT id,name_ru FROM `mb_category`";
		$categoriesList = $wpdb->get_results( $query );

		$query             = "SELECT id,name_ru FROM `sub_category`";
		$subCategoriesList = $wpdb->get_results( $query );
		?>
        <div>
            <h3>Редактирование товара <?= $product->name_ru; ?></h3>


            <style>
                .gallery{
                    width:100%;
                    float:left;
                    margin-top:15px;
                }
                .gallery ul{
                    margin:0;
                    padding:0;
                    list-style-type:none;
                }
                .gallery ul li{
                    padding:7px;
                    border:2px solid #ccc;
                    float:left;
                    margin:10px 7px;
                    background:none;
                    width:auto;
                    height:auto;
                }
                .gallery img{
                    height:250px;
                }
                .delProd{
                    position: absolute;
                    cursor: pointer;
                    color: red;
                    font-weight: bold;
                    font-size: 17pt;
                    margin-top: -17px;
                    margin-left: -9px;
                }
            </style>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

            <script>
                jQuery(document).ready(function () {
                    jQuery('.delProd').click(function () {
                        var del = jQuery(this).attr('id');
                        var delId = del.split('-');
                        var phId = delId[0];
                        var prod = delId[1];
                        jQuery.ajax({
                            type: 'POST',
                            url: '<?php echo admin_url( "admin-ajax.php" ) ?>',
                            data: {
                                action: 'delphoto',
                                idDel: phId,
                                prodID: prod
                            },
                            success: function(){
                                jQuery("#s").load(document.URL +  ' #s');
                            }
                        });
                        return false;
                    })
                })
            </script>
            <script>
                jQuery(function () {
                    var prod = jQuery('.prodID').val();
                    jQuery("ul.reorder-photos-list").sortable({
                        update: function f() {
                            var h = [];
                            jQuery("ul.reorder-photos-list li").each(function() {
                                h.push(jQuery(this).attr('id').substr(4));
                            });
                            jQuery.ajax({
                                type: "POST",
                                url: '<?php echo admin_url( "admin-ajax.php" ) ?>',
                                data: {
                                    action: 'photo',
                                    ids: "" + h + "",
                                    prod: prod},
                                success: function(){
                                    jQuery("#s").load(document.URL +  ' #s');
                                }
                            });
                            return false;

                    }
                });
                })

            </script>
            <div class="form-group" style="margin: 30px 0">
                <label>Фотографии товара:</label>
                <div class="row gallery">
                    <input type="hidden" class="prodID" value="<?= $product->id; ?>">

                    <ul class="reorder-photos-list" id="s">
			        <?php
			        if ( $product->photo ) {
				        $photos = unserialize( $product->photo );
				        $iter = 0;
				        foreach ( $photos as $photo ) {
					        ?>
                            <li id="img_<?= $iter; ?>" style="display: inline;">
                                <span id="<?= $iter; ?>-<?= $product->id; ?>-del" class="delProd">X</span>
                            <img src="<?php echo get_template_directory_uri() . '/productsImg/categories/' . $photo; ?>"
                                 style="height: 100px">
                            </li>
					        <?php
                            $iter++;
				        }
			        }
			        ?>
                    </ul>
                </div>
<!--                <label>Добавить фотографии <input type="file" name="img[]" multiple></label>-->
            </div>



            <form style="width: 90%" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label>Укажите категорию</label>
                    <select name="cat" id="" required>

						<?php
						foreach ( $categoriesList as $item ):
							?>
                            <option value="<?php echo $item->id; ?>" <?php if ( $item->id == $product->id_cat ) {
								echo 'selected="selected"';
							} ?>><?php echo $item->name_ru; ?></option>
						<?php
						endforeach;
						?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Укажите подкатегорию</label>
                    <select name="Subcat" id="subCat">
                        <option value=""></option>
						<?php
						foreach ( $subCategoriesList as $item ):
							?>
                            <option value="<?php echo $item->id; ?>" <?php if ( $item->id == $product->id_sub_cat ) {
								echo 'selected="selected"';
							} ?>><?php echo $item->name_ru; ?></option>
						<?php
						endforeach;
						?>
                    </select>
                </div>
                <div class="form-group">
                    <label>Название (рус.)<input type="text" name="ru_n" value="<?= $product->name_ru; ?>"
                                                 style="width: 100%"></label>
                    <label>Название (укр.)<input type="text" name="ua_n" value="<?= $product->name_ua; ?>"
                                                 style="width: 100%"></label>
                </div>

                <div class="form-group">
                    <label>Alt (для первого фото)</label><br>
                    <textarea name="alt" rows="3" style="width: 100%;"><?= $product->alt; ?></textarea>
                </div>
                <div class="form-group">
                    <label>Description (SEO описание товара)</label><br>
                    <textarea name="seo_descr" rows="5" style="width: 100%;"><?= $product->descr_seo; ?></textarea>
                </div>
                <div class="form-group">
                    <label>Описание товара</label><br>
					<?php wp_editor( $product->description, 'wpeditorprod', array( 'textarea_name' => 'descr' ) ); ?>
                </div>
                <div class="form-group">
                    <label>Бренд </label>
                    <select name="brand">
                        <option value=""></option>
						<?php
						foreach ( $brands as $item ):
							?>
                            <option value="<?php echo $item->id; ?>" <?php if ( $item->id == $product->brand ) {
								echo 'selected="selected"';
							} ?>><?php echo $item->name; ?></option>
						<?php
						endforeach;
						?>
                    </select>
                </div>
                <div class="form_group">
                    <label>Страна производитель <input type="text" name="country"
                                                       value="<?= $product->country; ?>"></label>
                </div>
                <div class="form-group">
                    <label>Цена <input type="text" name="price" value="<?= $product->price; ?>"></label>
                </div>
                <div class="form-group">
                    <label>Скидка в % <input type="text" name="sale" value="<?= $product->sale; ?>"></label>
                </div>
                <div class="form-group">
                    <label>Артикул <input type="text" name="article" value="<?= $product->article; ?>"></label>
                </div>
                <div class="form-group">
                    <?php
                    if ($product->size_list){
                        $sizesOld = unserialize($product->size_list);
                    }
                    if ($product->w_size){
                        $wSizesOld = unserialize($product->w_size);
                    }
                    ?>
                    <h6>Размеры</h6>
                    <p>
                        <label>XS<br><input type="checkbox" name="size[]" value="XS" <?php if ($sizesOld){if (in_array('XS',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>S<br><input type="checkbox" name="size[]" value="S" <?php if ($sizesOld){if (in_array('S',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>M<br><input type="checkbox" name="size[]" value="M" <?php if ($sizesOld){if (in_array('M',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>L<br><input type="checkbox" name="size[]" value="L" <?php if ($sizesOld){if (in_array('L',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>XL<br><input type="checkbox" name="size[]" value="XL" <?php if ($sizesOld){if (in_array('XL',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>2XL<br><input type="checkbox" name="size[]" value="2XL" <?php if ($sizesOld){if (in_array('2XL',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>3XL<br><input type="checkbox" name="size[]" value="3XL" <?php if ($sizesOld){if (in_array('3XL',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>4Xl<br><input type="checkbox" name="size[]" value="4Xl" <?php if ($sizesOld){if (in_array('4XL',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>5XL<br><input type="checkbox" name="size[]" value="5XL" <?php if ($sizesOld){if (in_array('5XL',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>6XL<br><input type="checkbox" name="size[]" value="6XL" <?php if ($sizesOld){if (in_array('6XL',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>7XL<br><input type="checkbox" name="size[]" value="7XL" <?php if ($sizesOld){if (in_array('7XL',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>8XL<br><input type="checkbox" name="size[]" value="8XL" <?php if ($sizesOld){if (in_array('8XL',$sizesOld)){echo 'checked="checked"';}} ?>></label>
                    </p>

                    <p>
                        <label>W:</label>
                        <label>27<br><input type="checkbox" name="w[]" value="27" <?php if ($wSizesOld){if (in_array('27',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>28<br><input type="checkbox" name="w[]" value="28" <?php if ($wSizesOld){if (in_array('28',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>29<br><input type="checkbox" name="w[]" value="29" <?php if ($wSizesOld){if (in_array('29',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>30<br><input type="checkbox" name="w[]" value="30" <?php if ($wSizesOld){if (in_array('30',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>31<br><input type="checkbox" name="w[]" value="31" <?php if ($wSizesOld){if (in_array('31',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>32<br><input type="checkbox" name="w[]" value="32" <?php if ($wSizesOld){if (in_array('32',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>33<br><input type="checkbox" name="w[]" value="33" <?php if ($wSizesOld){if (in_array('33',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>34<br><input type="checkbox" name="w[]" value="34" <?php if ($wSizesOld){if (in_array('34',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>36<br><input type="checkbox" name="w[]" value="36" <?php if ($wSizesOld){if (in_array('36',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>38<br><input type="checkbox" name="w[]" value="38" <?php if ($wSizesOld){if (in_array('38',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>40<br><input type="checkbox" name="w[]" value="40" <?php if ($wSizesOld){if (in_array('40',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>42<br><input type="checkbox" name="w[]" value="42" <?php if ($wSizesOld){if (in_array('42',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>44<br><input type="checkbox" name="w[]" value="44" <?php if ($wSizesOld){if (in_array('44',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>46<br><input type="checkbox" name="w[]" value="46" <?php if ($wSizesOld){if (in_array('46',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>48<br><input type="checkbox" name="w[]" value="48" <?php if ($wSizesOld){if (in_array('48',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>50<br><input type="checkbox" name="w[]" value="50" <?php if ($wSizesOld){if (in_array('50',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>52<br><input type="checkbox" name="w[]" value="52" <?php if ($wSizesOld){if (in_array('52',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>54<br><input type="checkbox" name="w[]" value="54" <?php if ($wSizesOld){if (in_array('54',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>56<br><input type="checkbox" name="w[]" value="56" <?php if ($wSizesOld){if (in_array('56',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>58<br><input type="checkbox" name="w[]" value="58" <?php if ($wSizesOld){if (in_array('58',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>60<br><input type="checkbox" name="w[]" value="60" <?php if ($wSizesOld){if (in_array('60',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>62<br><input type="checkbox" name="w[]" value="62" <?php if ($wSizesOld){if (in_array('62',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>64<br><input type="checkbox" name="w[]" value="64" <?php if ($wSizesOld){if (in_array('64',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                        <label>66<br><input type="checkbox" name="w[]" value="66" <?php if ($wSizesOld){if (in_array('66',$wSizesOld)){echo 'checked="checked"';}} ?>></label>
                    </p>
                </div>
                <div class="form-group">
					<?php
					if ( $product->available == 1 ) {
						$avail = 'checked="checked"';
					} else {
						$avail = '';
					}
					?>
                    <label for="">В наличии <input type="checkbox" name="available" <?= $avail; ?>></label>
                </div>
                <div class="form-group">
					<?php
					if ( $product->showcase == 1 ) {
						$showc = 'checked="checked"';
					} else {
						$showc = '';
					}
					?>
                    <label for="">Добавить на витрину <input type="checkbox" name="showcase" <?= $showc; ?>></label>
                </div>
                <input type="hidden" value="<?= $back;?>" name="back">
                <button type="submit" class="btn btn-success" name="editProd" value="<?= $product->id; ?>">Сохранить
                </button>
            </form>
        </div>

		<?php
	}

}

function editProdProf() {


	//-------------------------//

$array='';

    //------------------------//


	global $wpdb;

    if (isset($_POST['do'])){
	    $cat    = $_POST['cat'];
		$subCat = $_POST['Subcat'];
		if ( $subCat ) {
			$query  = "SELECT post_id FROM `sub_category` WHERE id='$subCat'";
			$parent = $wpdb->get_row( $query );
			$parent = $parent->post_id;
		} else {
			$query  = "SELECT post_id FROM `mb_category` WHERE id='$cat'";
			$parent = $wpdb->get_row( $query );
			$parent = $parent->post_id;
		}
	    foreach ( $array as $item ) {

		    $images = array();
		    $arr2 = preg_replace('/\,\s/',PHP_EOL,$item[5]);
		    $arr2 = explode(PHP_EOL,$arr2);

		    $iterat = 0;
		    foreach ($arr2 as $arr){
			    $img      = date( 'y-m-d--h-m-s-' ) . $iterat . rand(1,800) . '.jpg';

				    $cat_dir    = $cat . '/';
				    $uploaddir  = '../wp-content/themes/twentynineteen/productsImg/categories/' . $cat_dir;
				    $uploadfile = $uploaddir . basename( $img );
				    if ( ! is_dir( $uploaddir ) ) {
					    mkdir( $uploaddir );
				    }
                    file_put_contents($uploadfile, file_get_contents($arr));
				    array_push( $images, $cat_dir . basename( $img ) );
                    $iterat ++;
            }
		    $images = serialize( $images );

		    $query = "INSERT INTO `mb_products`(id_cat, id_sub_cat, name_ru, article, price, sale, photo, description, descr_seo, country, available) VALUES ('$cat','$subCat','$item[1]','$item[0]','$item[4]','$item[8]','$images','$item[3]','$item[2]','$item[7]', 1)";
		    $wpdb->query( $query );
		    $lastId = $wpdb->insert_id;
		    $postName = $lastId.'-'.$item[1];

		    $page_check = get_page_by_title( transliterate( $postName ) );
		    $new_page   = array(
			    'post_type'    => 'page',
			    'post_title'   => $postName,
			    'post_name'    => transliterate( $postName ),
			    'post_content' => '',
			    'post_status'  => 'publish',
			    'post_author'  => 1,
			    'post_parent'  => $parent,
		    );
		    if ( ! isset( $page_check->ID ) ) {
			    $new_page_id = wp_insert_post( $new_page );
			    update_post_meta( $new_page_id, '_wp_page_template', 'product.php' );
			    $page  = get_post( $new_page_id );
			    $slug  = $page->post_name . '/';
			    $query = "UPDATE `mb_products` SET url_ru='$slug' WHERE id='$lastId'";
			    $wpdb->query( $query );
		    }
	    }
    }



	$query          = "SELECT id,name_ru FROM `mb_category`";
	$categoriesList = $wpdb->get_results( $query );

	$query             = "SELECT id,name_ru FROM `sub_category`";
	$subCategoriesList = $wpdb->get_results( $query );
?>
<form action="" method="post">
        <div class="row">

            <div class="form-group">
                <label>Укажите категорию</label>
                <select name="cat" id="" required>
                    <option selected="selected"></option>
			        <?php
			        foreach ( $categoriesList as $item ):
				        ?>
                        <option value="<?php echo $item->id; ?>"><?php echo $item->name_ru; ?></option>
			        <?php
			        endforeach;
			        ?>
                </select>
            </div>
            <div class="form-group">
                <label>Укажите подкатегорию</label>
                <select name="Subcat" id="subCat">
                    <option selected="selected"></option>
			        <?php
			        foreach ( $subCategoriesList as $item ):
				        ?>
                        <option value="<?php echo $item->id; ?>"><?php echo $item->name_ru; ?></option>
			        <?php
			        endforeach;
			        ?>
                </select>
            </div>

        </div>

        <button name="do" class="btn" disabled="disabled">GO</button>
    </form>

    <div>
        <h3>Files, with 404 code status:</h3>
        <p>https://images.ua.prom.st/1189765645_stilnaya-muzhskaya-futbolka.jpg</p>
        <p>https://images.ua.prom.st/1197715905_futbolka-muzhskaya-letnyaya.jpg</p>
        <p>https://images.ua.prom.st/1189781624_futbolka-muzhskaya-moschinoreplika-1218.jpg</p>
        <p>https://images.ua.prom.st/157861524_muzhskaya-tenniska-amato.jpg</p>
        <p>https://images.ua.prom.st/1177388568_letnyaya-tenniska-s.jpg</p>
        <p>https://images.ua.prom.st/176472926_dzhinsy-muzhskie-svetlyestilnye.jpg</p>
        <p>https://images.ua.prom.st/850376967_dzhinsy-muzhskie-svetlyestilnye.jpg</p>
        <p>https://images.ua.prom.st/850205998_dzhinsy-muzhskie-ryzhiestilnye.jpg</p>
        <p>https://images.ua.prom.st/202912965_shikarnye-sergi-nezhnosts.jpg</p>
        <p>https://images.ua.prom.st/201525949_shikarnyj-zhenskij-braslet.jpg</p>
        <p>https://images.ua.prom.st/884367347_sviterakardigany-osen-zimakachestvo.jpg</p>
        <p>https://images.ua.prom.st/126568274_modnyj-stilnyj-reglanck.jpg</p>
    </div>
	<?php
}

function prodList()
{

?>
    <div class="col-12 row" style="margin: 10px auto;">
        <div class="col-6">
            <form action="" method="get">
                <input type="hidden" name="page" value="prod-list">
                <label>Введите артикул для поиска: </label><input type="text" name="article" value="">
                <button class="btn btn-outline-success">Найти</button>
            </form>
        </div>
        <div class="col-6">
            <form action="" method="get">
                <input type="hidden" name="page" value="prod-list">
                <button class="btn btn-outline-success">Сбросить результаты поиска</button>
            </form>
        </div>
    </div>
    <hr>

    <?php

    global $wpdb;
    $idSubCat = $_GET['idSub'];
    $idCat = $_GET['idCat'];
    $article = $_GET['article'];
    $perPage = 50;
    $numStr = ($_GET['num']*$perPage)-$perPage;

    if ($idSubCat){
	    $query = "SELECT COUNT(*) FROM `mb_products` WHERE id_sub_cat='$idSubCat'";
	    $sumProd = $wpdb->get_row($query,ARRAY_N);
    } elseif ($idCat){
	    $query = "SELECT COUNT(*) FROM `mb_products` WHERE id_cat='$idCat'";
	    $sumProd = $wpdb->get_row($query,ARRAY_N);
    } elseif ($article){
	    $query = "SELECT COUNT(*) FROM `mb_products` WHERE article LIKE '%$article%'";
	    $sumProd = $wpdb->get_row($query,ARRAY_N);
    }
    else {
	    $query = "SELECT COUNT(*) FROM `mb_products`";
	    $sumProd = $wpdb->get_row($query,ARRAY_N);
    }
    $sumProd = $sumProd[0];
    $pageCount = ceil($sumProd/$perPage);
    $nowPageIs = $_GET['num'];
    if (!$nowPageIs){$nowPageIs=1;}


    if ($numStr<0){$numStr = 0;}
    if ($idSubCat){
	    $query = "SELECT * FROM `mb_products` WHERE id_sub_cat='$idSubCat' ORDER BY article ASC LIMIT $numStr,$perPage";
    } elseif ($idCat){
	    $query = "SELECT * FROM `mb_products` WHERE id_cat='$idCat' ORDER BY article ASC LIMIT $numStr,$perPage";
    } elseif ($article){
        $query = "SELECT * FROM `mb_products` WHERE article LIKE '%$article%'";
    }
    else {
	    $query = "SELECT * FROM `mb_products` ORDER BY article ASC LIMIT $numStr,$perPage";
    }
    $list = $wpdb->get_results($query);

	$query  = "SELECT * FROM `mb_brands` ORDER BY id";
	$brands = $wpdb->get_results( $query );

	$query          = "SELECT id,name_ru FROM `mb_category` ORDER BY id";
	$categoriesList = $wpdb->get_results( $query );

	$query             = "SELECT id,name_ru FROM `sub_category` ORDER BY id";
	$subCategoriesList = $wpdb->get_results( $query );


    foreach ($list as $item){
        $photo = unserialize($item->photo);
    ?>

        <div class="col-12 row" style="border-bottom: black solid 1px">
            <div class="col-2">
                <img src="<?php echo get_template_directory_uri(); ?>/productsImg/categories/<?php echo $photo[0]; ?>" alt="" width="100">
            </div>
            <div class="col-2">
                <strong>Дата:</strong><br> <?php echo date_create($item->date)->Format('d.m.Y');?>
            </div>
            <div class="col-2">
                <strong>Артикул:</strong> <?php echo $item->article; ?><br>
                <strong>Название:</strong> <?php echo $item->name_ru; ?><br>
                <strong>Бренд:</strong> <?php
	            foreach ($brands as $brand){
		            if ($brand->id == $item->brand){
			            echo $brand->name;
		            }
	            }
                ?>
            </div>
            <div class="col-2">
                <strong>Категория:</strong> <?php
	            foreach ($categoriesList as $cat){
		            if ($cat->id == $item->id_cat){
			            echo $cat->name_ru;
		            }
	            }
	            ?><br>
                <strong>Подкатегория:</strong> <?php
	            foreach ($subCategoriesList as $subCat){
		            if ($subCat->id == $item->id_sub_cat){
			            echo $subCat->name_ru;
		            }
	            }
	            ?>
            </div>
            <div class="col-2">
                <strong>Цена:</strong> <?php echo $item->price; ?>грн.<br>
                <strong>Скидка:</strong> <?php echo $item->sale;?>% <br>
                <?php
                if ($item->available == 1){
                    echo '<span style="color: green">Товар в наличии</span>';
                } else echo '<span style="color: red">Товар отсутствует</span>';
                ?><br>
	            <?php
	            if ($item->showcase == 1){
		            echo '<span style="color: green">Товар на витрине</span>';
	            } else echo '<span style="color: red">Товар не выводится на витрине</span>';
	            ?>
            </div>
            <div class="col-2">

                <form action="" method="get">
                    <input type="hidden" name="back" value="<?php
                    $url = explode('?',$_SERVER['REQUEST_URI']);
                    echo $url[1];
                    ?>">
                    <input type="hidden" name="page" value="edit-prod">
                    <input type="hidden" name="id" value="<?php echo $item->id;?>">
                    <button class="btn btn-outline-success">Редактировать</button>
                </form>
                <button id="<?= $item->id;?>-del" class="btn btn-outline-success delProd">Удалить</button>
            </div>
        </div>
<?php
    }

    if ($pageCount == 1){
        ?>
        <div class="col-12" style="text-align: center"><p>Страница 1 из 1</p></div>
	    <?php
    }
	if ( $pageCount > 1 ) {
		?>
        <div class="col-3 offset-6 row mx-auto" style="text-align: center; margin-top: 10px">
			<?php
			if ( $nowPageIs != 1 ) {
				?>
                <form action="" method="get">
                    <input type="hidden" name="num" value="<?= $nowPageIs - 1; ?>">
                    <input type="hidden" name="page" value="prod-list">
                    <?php
                    if ($idCat){
                        ?>
                        <input type="hidden" name="idCat" value="<?= $idCat; ?>">
	                    <?php
                    } elseif ($idSubCat){
                        ?>
                        <input type="hidden" name="idSub" value="<?= $idSubCat; ?>">

	                    <?php
                    }
                    ?>
                    <button class="btn btn-sm"
                            style="color: #7cefbc; border: solid 1px #7cefbc; background-color: white; font-weight: 600">
                        <
                    </button>
                </form>
				<?php
			}
			?>
            <p style="margin: auto 10px;">
                Страница <?= $nowPageIs; ?> из <?= $pageCount; ?>
            </p>
			<?php
			if ( $nowPageIs + 1 <= $pageCount ) {
				?>
                <form action="" method="get">
                    <input type="hidden" name="page" value="prod-list">
                    <input type="hidden" name="num" value="<?= $nowPageIs + 1; ?>">
	                <?php
	                if ($idCat){
		                ?>
                        <input type="hidden" name="idCat" value="<?= $idCat; ?>">
		                <?php
	                } elseif ($idSubCat){
		                ?>
                        <input type="hidden" name="idSub" value="<?= $idSubCat; ?>">

		                <?php
	                }
	                ?>
                    <button class="btn btn-sm"
                            style="color: #7cefbc; border: solid 1px #7cefbc; background-color: white; font-weight: 600">
                        >
                    </button>
                </form>
				<?php
			}
			?>
        </div>
        <div class="col-4 offset-4 row mx-auto" style="text-align: center; margin-top: 20px">
            <form action="" method="get">
                <input type="hidden" name="page" value="prod-list">
		        <?php
		        if ($idCat){
			        ?>
                    <input type="hidden" name="idCat" value="<?= $idCat; ?>">
			        <?php
		        } elseif ($idSubCat){
			        ?>
                    <input type="hidden" name="idSub" value="<?= $idSubCat; ?>">

			        <?php
		        }
		        ?>
                <label>Перейти на страницу: <input type="number" name="num" style="width: 70px"></label>

                <button class="btn btn-sm"
                        style="color: #7cefbc; border: solid 1px #7cefbc; background-color: white; font-weight: 600; margin-bottom: 7px;">
                    Перейти
                </button>
            </form>
        </div>
		<?php

	}
    ?>


    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

    <script>
        jQuery(document).ready(function () {
            jQuery('.delProd').click(function () {
                var del = jQuery(this).attr('id');
                var delId = del.split('-');
                delId = delId[0];
                jQuery.ajax({
                    type: 'POST',
                    url: '<?php echo admin_url("admin-ajax.php") ?>',
                    data: 'action=fulldelprod&idDel='+delId,
                });
                setTimeout(function () {
                    location.reload(true);
                }, 1000);

                return false;
            })
        })
    </script>
    <?php
}

function categorList()
{
    global $wpdb;
$query = "SELECT * FROM `mb_category` ORDER BY priority ASC";
$categories = $wpdb->get_results($query);
?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>


        <script type="text/javascript">
            jQuery(document).ready(function () {
                jQuery('#s').sortable({
                    update: function () {
                        var h = [];
                        jQuery("#s div.sort").each(
                            function () {
                                h.push(jQuery(this).attr('id'));
                            });
                        // console.log(h);
                        jQuery.ajax({
                           type: "POST",
                            url: '<?php echo admin_url( "admin-ajax.php" ) ?>',
                           data: {
                               action: 'categorySort',
                               ids: "" + h + "",
                           },
                           success: function(){
                               // window.location.reload(true);
                               jQuery("#s").load(document.URL +  ' #s');
                           }
                        });
                        return false;
                    }
                });
            })
        </script>
    <div class="col-12">
        <div class="row col-12" style="background-color: #1d2124; color: whitesmoke; font-size: 12pt; text-align: center">
            <div class="col-1">
                Фото
            </div>
            <div class="col-2">
                Название (рус)
            </div>
            <div class="col-2">
                Название (укр)
            </div>
            <div class="col-4">
                Подкатегории
            </div>
            <div class="col-1">
                Товаров в категории
            </div>
            <div class="col-2">
                Действия
            </div>
        </div>

        <div class="col-12" id="s" style="padding: 0">
		<?php
		foreach ($categories as $category):
			?>
            <div class="row col-12 sort" id="<?php echo $category->id; ?>" style="border-bottom: 1px solid #1d2124; padding: 10px 0;">
                <div class="col-1"><img src="<?php echo get_template_directory_uri().'/productsImg/categories/'.$category->photo; ?>" style="width: 80px"></div>
                <div class="col-2"><?php echo $category->name_ru; ?></div>
                <div class="col-2"><?php echo $category->name_ua; ?></div>
                <div class="col-4"><?php
	                $query = "SELECT id,name_ru FROM `sub_category` WHERE id_cat='$category->id' ORDER BY priority ASC";
	                $subcategories = $wpdb->get_results($query);
	                if ($subcategories){
	                    foreach ($subcategories as $item) {
		                    ?>
                            <form action="" method="get">
                                <input type="hidden" name="page" value="prod-list">
                                <input type="hidden" name="idSub" value="<?php echo $item->id;?>">
                                <button class="btn btn-outline-success"><?php echo $item->name_ru?></button>
                            </form>
		                    <?php
	                    }
                    }
                    ?></div>
                <div class="col-1">
                    <?php
                    $query = "SELECT COUNT(*) FROM `mb_products` WHERE id_cat='$category->id'";
                    $count = $wpdb->get_row( $query, ARRAY_N );

                    echo 'Всего: '.$count[0]."шт.";
                    ?>
                </div>
                <div class="col-2">
                    <form action="" method="get">
                        <input type="hidden" name="page" value="edit-category">
                        <input type="hidden" name="id" value="<?php echo $category->id;?>">
                        <button class="btn btn-outline-success">Редактировать</button>
                    </form>
                    <form action="" method="get">
                        <?php
                        if($subcategories){
                            ?>
                            <input type="hidden" name="page" value="subCategory-list">
	                        <?php
                        } else {
                            ?>
                            <input type="hidden" name="page" value="prod-list">
	                        <?php
                        }
                        ?>
                        <input type="hidden" name="idCat" value="<?php echo $category->id;?>">
                        <button class="btn btn-outline-success">Открыть категорию</button>
                    </form>
                </div>
            </div>
		<?php endforeach; ?>
        </div>
    </div>
<?php
}

function subCategoryList()
{
	global $wpdb;
	if ($_GET['id']){
	    $id = $_GET['id'];
		$query = "SELECT * FROM `sub_category` WHERE id_cat='$id' ORDER BY priority ASC";
	} else {
		$query = "SELECT * FROM `sub_category` ORDER BY priority ASC";

	}
	$categories = $wpdb->get_results($query);

	?>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <table class="table table-striped table-hover">
        <thead class="thead-dark">
        <tr>
            <th>Фото</th>
            <th>Основная категория</th>
            <th>Название подкатегории (рус)</th>
            <th>Название подкатегории (укр.)</th>
            <th>Товаров в подкатегории</th>
            <th>Действия</th>
        </tr>
        </thead>
        <tbody>
		<?php
        if($id){
            $query = "SELECT name_ru FROM `mb_category` WHERE id='$id'";
            $catName = $wpdb->get_row($query);
            $catName = $catName->name_ru;
        }
		foreach ($categories as $category):
			?>
<!--            <form action="--><?php //echo plugins_url('Categories/edit.php'); ?><!--" method="post">-->
                <tr id="<?php echo $category->id; ?>">
                    <td><img src="<?php echo get_template_directory_uri().'/productsImg/categories/'.$category->photo; ?>" style="width: 80px"></td>
                    <td><?php
                        if ($catName){
                            echo $catName;
                        } else {
                            $query = "SELECT name_ru FROM `mb_category` WHERE id='$category->id_cat'";
                            $catName = $wpdb->get_row($query);
                            $catName = $catName->name_ru;
	                        echo $catName;
                        }
                       ?></td>
                    <td><?php echo $category->name_ru; ?></td>
                    <td><?php echo $category->name_ua; ?></td>
                    <td>
		                <?php
		                $query = "SELECT COUNT(*) FROM `mb_products` WHERE id_sub_cat='$category->id'";
		                $count = $wpdb->get_row( $query, ARRAY_N );

		                echo 'Всего: '.$count[0]."шт.";
		                ?>
                    </td>
                    <td>
                        <form action="" method="get">
                            <input type="hidden" name="page" value="edit-subcategory">
                            <input type="hidden" name="id" value="<?php echo $category->id;?>">
                            <button class="btn btn-outline-success">Редактировать</button>
                        </form>
                        <form action="" method="get">
                            <input type="hidden" name="page" value="prod-list">
                            <input type="hidden" name="idSub" value="<?php echo $category->id;?>">
                            <button class="btn btn-outline-success">Открыть подкатегорию</button>
                        </form>
                    </td>
                </tr>
<!--            </form>-->

		<?php endforeach; ?>
        </tbody>
    </table>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    <script type="text/javascript">
        jQuery('tbody').sortable();
    </script>
	<?php
}
function addSubCategory()
{
	?>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<?php
	global $wpdb;
	if (isset($_POST['add'])){

		$name_ua = clean($_POST['ua_n']);
		$name_ru = clean($_POST['ru_n']);
		$alt = clean($_POST['alt']);
		$descr_seo = clean($_POST['seo_descr']);
		$descr = $_POST['descr'];
		$parentCat = $_POST['cat'];
		$descr_d = $_POST['descr_down'];

		if ($_FILES['img']['error'] == 0) {
			$type = explode(".", $_FILES['img']['name']);
			$img =  date('y-m-d-h-m-s') .rand(0,50)."-subcategory." . $type[1];
			$tmp_path = $_FILES['img']['tmp_name'];
			if (is_image($tmp_path) == true) {
				$uploaddir = '../wp-content/themes/twentynineteen/productsImg/categories/';
				$uploadfile = $uploaddir . basename($img);
				if (move_uploaded_file($tmp_path, $uploadfile)) {
					$image = $img;
				} else
					echo "Выберите картинку!";
			} else
				echo "<script>alert('Загруженный файл не соответствует формату картинки');</script>";
		}

		$queryPriority = "SELECT MAX(`priority`) AS MaxPrioroty FROM `sub_category`";
		$priority = $wpdb->get_row($queryPriority);
		$priority = $priority->MaxPrioroty + 1;

		$slug_ru = transliterate($name_ru);
		$query = "SELECT post_id FROM `mb_category` WHERE id='$parentCat'";
		$parentPostId = $wpdb->get_row($query);
		$parentPostId = $parentPostId->post_id;

		$page_check = get_page_by_title(transliterate($name_ru));
		$new_page = array(
			'post_type' => 'page',
			'post_title' => $name_ru,
			'post_name'  => $slug_ru,
			'post_content' => '',
			'post_status' => 'publish',
			'post_author' => 1,
			'post_parent' => $parentPostId,
		);
		if(!isset($page_check->ID)){
			$new_page_id = wp_insert_post($new_page);
			update_post_meta($new_page_id, '_wp_page_template', 'subCategory.php');
			$page = get_post($new_page_id);
			$slug = $page->post_name.'/';

			$query = "INSERT INTO `sub_category` (name_ru,alt,description,priority,active,name_ua,seo_descr,photo,id_cat,descr_d,url_ru, post_id,parent_post_id) VALUES ('$name_ru','$alt','$descr','$priority',1,'$name_ua','$descr_seo','$image','$parentCat','$descr_d','$slug','$new_page_id','$parentPostId')";
			if ($wpdb->query( $query )){
				echo "<script>Swal.fire(
  'Успех!',
  'Подкатегория добавлена!',
  'success'
)</script>";
			}
		}

	}
	$query = "SELECT id,name_ru FROM `mb_category`";
	$categoriesList = $wpdb->get_results($query);
	?>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <div>
        <h3>Добавление подкатегории</h3>
        <form style="width: 50%" method="post" enctype="multipart/form-data">
            <div>
                <label>Укажите основную категорию</label>
                <select name="cat" id="">
					<?php
					foreach ($categoriesList as $item):
						?>
                        <option value="<?php echo $item->id; ?>"><?php echo $item->name_ru; ?></option>
					<?php
					endforeach;
					?>
                </select>
            </div>
            <div class="form-group">
                <label>Название (рус.)<input type="text" name="ru_n" required></label>
                <label>Название (укр.)<input type="text" name="ua_n" required></label>
            </div>
            <div class="form-group">
                <label><input type="file" name="img" id="inputFile" required></label>
                <img id="image_upload_preview" src="http://placehold.it/200x200" alt="your image" width="200px"/>
            </div>
            <div class="form-group">
                <label>Alt (для фото)</label><br>
                <textarea name="alt" id="" cols="60" rows="5" style="width: 100%;"></textarea>
            </div>
            <div class="form-group">
                <label>Description (SEO описание категории)</label><br>
                <textarea name="seo_descr" id="" cols="60" rows="5" style="width: 100%;"></textarea>
            </div>
            <div class="form-group">
                <label>Описание сверху</label><br>
				<?php wp_editor( '', 'wpeditorsubcat', array('textarea_name' => 'descr') ); ?>
            </div>
            <div class="form-group">
                <label>Описание снизу</label><br>
				<?php wp_editor( '', 'wpeditorsubcatdown', array('textarea_name' => 'descr_down') ); ?>
            </div>
            <button type="submit" class="btn btn-success" name="add">Сохранить</button>
        </form>
    </div>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    jQuery('#image_upload_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        jQuery("#inputFile").change(function () {
            readURL(this);
        });
    </script>

	<?php

}
function addCategory()
{
	global $wpdb;
	if (isset($_POST['add'])){
		$name_ua = clean($_POST['ua_n']);
		$name_ru = clean($_POST['ru_n']);
		$alt = clean($_POST['alt']);
		$descr_seo = clean($_POST['seo_descr']);
		$descr = $_POST['descr'];
		$descr_d = $_POST['descr_down'];

		if ($_FILES['img']['error'] == 0) {
			$type = explode(".", $_FILES['img']['name']);
			$img =  transliterate($name_ua) . "category." . $type[1];
			$tmp_path = $_FILES['img']['tmp_name'];
			if (is_image($tmp_path) == true) {
				$uploaddir = '../wp-content/themes/twentynineteen/productsImg/categories/';
				$uploadfile = $uploaddir . basename($img);
				if (move_uploaded_file($tmp_path, $uploadfile)) {
					$image = $img;
				} else
					echo "Выберите картинку!";
			} else
				echo "<script>alert('Загруженный файл не соответствует формату картинки');</script>";
		}

		$queryPriority = "SELECT MAX(`priority`) AS MaxPrioroty FROM `mb_category`";
		$priority = $wpdb->get_row($queryPriority);
		$priority = $priority->MaxPrioroty + 1;

		$slug_ru = transliterate($name_ru);

		$page_check = get_page_by_title(transliterate($name_ru));
		$new_page = array(
			'post_type' => 'page',
			'post_title' => $name_ru,
			'post_name'  => $slug_ru,
			'post_content' => '',
			'post_status' => 'publish',
			'post_author' => 1,
		);
		if(!isset($page_check->ID)){
			$new_page_id = wp_insert_post($new_page);
			update_post_meta($new_page_id, '_wp_page_template', 'category.php');
			$page = get_post($new_page_id);
			$slug = $page->post_name.'/';

			$query = "INSERT INTO `mb_category` (name_ru,alt,description,priority,active,name_ua,seo_descr,photo,descr_d,post_id,url_ru) VALUES ('$name_ru','$alt','$descr','$priority',1,'$name_ua','$descr_seo','$image','$descr_d','$new_page_id','$slug')";
			$wpdb->query($query);
		}


	}

	?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <div>
        <h3>Добавление категории</h3>
        <form style="width: 50%" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label>Название (рус.)<input type="text" name="ru_n" required></label>
                <label>Название (укр.)<input type="text" name="ua_n" required></label>
            </div>
            <div class="form-group">
                <label>Фотография <input type="file" name="img" id="inputFile"></label>
                <img id="image_upload_preview" src="http://placehold.it/100x100" alt="your image" width="200px"/>
            </div>
            <div class="form-group">
                <label>Alt (для фото)</label><br>
                <textarea name="alt" id="" cols="60" rows="5" style="width: 100%;"></textarea>
            </div>
            <div class="form-group">
                <label>Description (SEO описание категории)</label><br>
                <textarea name="seo_descr" id="" cols="60" rows="5" style="width: 100%;"></textarea>
            </div>
            <div class="form-group">
                <label>Описание (вверху)</label><br>
				<?php wp_editor( '', 'wpeditorcat', array('textarea_name' => 'descr') ); ?>
            </div>
            <div class="form-group">
                <label>Описание (снизу)</label><br>
				<?php wp_editor( '', 'wpeditorcatdown', array('textarea_name' => 'descr_down') ); ?>
            </div>


            <button type="submit" class="btn btn-success" name="add">Сохранить</button>
        </form>
    </div>
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    jQuery('#image_upload_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        jQuery("#inputFile").change(function () {
            readURL(this);
        });
    </script>
	<?php
}

function register_prod_menu_page() {
	add_menu_page( 'Категории:', 'Категории товаров:', 'manage_options', 'categor-list', 'categorList' );
	add_submenu_page( 'categor-list', 'Подкатегории', 'Список всех подкатегорий', 'manage_options', 'subCategory-list', 'subCategoryList' );
	add_submenu_page( 'categor-list', 'Список товаров', 'Список всех товаров', 'manage_options', 'prod-list', 'prodList' );

	add_submenu_page( 'categor-list','Добавить товар', 'Добавить товар', 'manage_options', 'add-prod', 'addProducts' );
	add_submenu_page('categor-list', 'Добавить категорию', 'Добавить категорию', 'manage_options', 'add-category', 'addCategory');
	add_submenu_page('categor-list', 'Добавить подкатегорию', 'Добавить подкатегорию', 'manage_options', 'add-subcategory', 'addSubCategory');

	add_submenu_page( 'categor-list', 'Добавить програмно', 'Добавить програмно', 'manage_options', 'edit-prod-prof', 'editProdProf' );

	add_submenu_page( 'categor-list', 'Редактировать товар', 'Редактировать товар', 'manage_options', 'edit-prod', 'editProd' );

}

add_action( 'admin_menu', 'register_prod_menu_page' );
