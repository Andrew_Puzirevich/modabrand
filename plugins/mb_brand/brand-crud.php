<?php
/**
 * Plugin Name: MB Brands
 * Description: crud brands
 * Author: Andrew Puzirevich
 * Version: 1.0
 */

function brandCRUD()
{

	require_once( ABSPATH . 'wp-admin/includes/image.php' );
	global $wpdb;
	$wp_upload_dir = wp_upload_dir();
	$sliderOption = get_post_meta(173,'sp_wpcp_upload_options');

	$url = str_replace(home_url(),'..',$wp_upload_dir['url']);
	$img = '';
	if (isset($_POST['add'])){
		$bName = $_POST['brand'];
		$bDescription = $_POST['descr'];

		if ($_FILES['img']['error'] == 0) {
			$type = explode(".", $_FILES['img']['name']);
			$img =  transliterate($bName) . "." . $type[1];
			$tmp_path = $_FILES['img']['tmp_name'];
			if (is_image($tmp_path) == true) {
				$uploaddir = $url . '/';
				$uploadfile = $uploaddir . basename($img);
				if (move_uploaded_file($tmp_path, $uploadfile)) {
					$image = $img;
				}
			}
		}
		$filename = $url.'/'.$image;
		$filetype = wp_check_filetype( basename( $filename ), null );


		$page_check = get_page_by_title(transliterate($bName));
		$new_page = array(
			'post_type' => 'page',
			'post_title' => $bName,
			'post_name'  => transliterate($bName),
			'post_content' => '',
			'post_status' => 'publish',
			'post_author' => 1,
		);
		if(!isset($page_check->ID)){
			$new_page_id = wp_insert_post($new_page);
			update_post_meta($new_page_id, '_wp_page_template', 'brand.php');
			$page = get_post($new_page_id);
			//
			$attachment = array(
				'guid'           => $wp_upload_dir['url'] . '/' . basename( $img ),
				'post_mime_type' => $filetype['type'],
				'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $img ) ),
				'post_content'   => '',
				'post_status'    => 'inherit',
				'post_excerpt'   => '/'.$page->post_name
		);
			$attach_id = wp_insert_attachment( $attachment, $filename );
			$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
			wp_update_attachment_metadata( $attach_id, $attach_data );
			$sliderOption[0]['wpcp_gallery'] .= ','.$attach_id;
			delete_post_meta(173,'sp_wpcp_upload_options');
			add_post_meta(173,'sp_wpcp_upload_options',$sliderOption[0]);

			$slug = $page->post_name.'/';
			$query = "INSERT INTO `mb_brands` (`name`,url_ru,description) VALUES ('$bName','$slug','$bDescription')";
			if ($wpdb->query($query)){
				?>
				<script>
                    alert('Брэнд успешно создан!');
				</script>
				<?php
			}

		}

	}
	?>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	        crossorigin="anonymous"></script>
	<form method="post" enctype="multipart/form-data">
		<h3>Загрузите картинку</h3>
		<img id="image_upload_preview" src="http://placehold.it/100x100" alt="your image" width="100px"/>
		<input type="file" name="img" id="inputFile">
		<h3>Введите название бренда</h3>
		<input type="text" name="brand">
		<h3>Введите описание</h3>
		<p><textarea name="descr" id="" rows="10" style="width: 50%"></textarea></p>
		<button style="text-decoration: none;font-weight: 600; margin: 10px; color: #7cefbc; border: solid 1px #7cefbc; background-color: white; padding: 10px; -webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px; font-size: 20px;" name="add">Создать брэнд</button>
	</form>

	<script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    jQuery('#image_upload_preview').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        jQuery("#inputFile").change(function () {
            readURL(this);
        });
	</script>
	<?php
}

function register_mbbrand_menu_page()
{
	add_menu_page('Бренды','Бренды', 'manage_options', 'MBbrands', 'brandCRUD');
}
add_action( 'admin_menu', 'register_mbbrand_menu_page' );
